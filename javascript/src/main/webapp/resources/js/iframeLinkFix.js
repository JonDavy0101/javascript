(() => {
  document.querySelectorAll("iframe").forEach(iframe => {
    iframe.addEventListener("load", e => {
      iframe.contentDocument.addEventListener("click", e => {
        if (e.target.parentNode) {
          if (
            e.target.parentNode.nodeName === "A" &&
            e.target.parentNode.href
          ) {
            e.preventDefault();
          }
          if (
            e.target.parentNode.querySelector("a") &&
            e.target.parentNode.querySelector("a").href
          ) {
            e.preventDefault();
          }
        }
        if (e.target.nodeName === "A" && e.target.href) {
          e.preventDefault();
        }
        if (e.target.querySelector("a") && e.target.querySelector("a").href) {
          e.preventDefault();
        }
        if (e.detail && e.detail.href) {
          e.preventDefault();
        }
      });
    });
  });
})();
