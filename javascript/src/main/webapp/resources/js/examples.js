
var index = 0;
var exampleSize;
var responseDiv;
var nextElem;
var prevElem;
var textareaArr = [];
var editorsArr = [];
var outputElem;
var inputElem;

orly.ready.then((e)=>{
//	console.log("DOM READY!");
//	let sidebar = orly.q(".sidebar-sticky");
//	console.log(`sidebar ${sidebar}`);
//	
//	if (sidebar) {
//		orly.on(sidebar, "click", (e)=>{
//			console.log("sidebar clicked!");
//		});
//	}
});

function toast(type, message) {
	var alertManager = orly.qid("alerts");
	alertManager.createAlert({type:type, duration:"2000", msg:message});
}

function printError(err) {
	print(err);
	/*
    if (err instanceof ReferenceError) {
        printObject(`ReferenceError=> ${err}`);
    } else {
	    //print("Check the console for errors.");
	    //console.log(err);
    	print(err);
    }*/
}

function removeActiveItems() {
	try {
		var elements = document.getElementsByTagName("orly-nav-item");
		
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			element.classList.remove("active");
		}		
	} catch (err) {
		console.log(`Handled Error: ${err}`);
	}
}

function setActiveMenuItem(page) {
	try {
		var elements = document.getElementsByTagName("orly-nav-item");
		
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			var attr = element.getAttribute("onclick");
			
			if (attr && attr.includes(page)) {
				element.classList.add("active");
			} else {
				element.classList.remove("active");
			}
		}	
	} catch (err) {
		console.log(`Handled Error: ${err}`);
	}
}

function intializePage() {
	index=0;
	initializeArrays();
	outputElem = orly.qid("output");
	prevElem = orly.qid("prev");
	nextElem = orly.qid("next");
	inputElem = orly.qid("input");
	showFirstExample();
	adjustTextareas();
	exampleSize = textareaArr.length;
	dialogSize = editorsArr.length;
	updateHeadingValue();
	addEventListeners();
	intializePrevAndNext();
	loadMenu("menu.html");
}

function showFirstExample() {
	if (textareaArr != undefined && textareaArr != null && textareaArr.length > 0) {
		var item = textareaArr[0];
		item.classList.remove("display-none");
		item.classList.remove("d-none");
	}
}

function loadMenu(filename) {
	try {
		//let menu = orly.qid("menu");
		//include(menu, filename);
	} catch (err) {
		console.log(err);
	}
}

function include(target, filename) {	
	fetch(filename)
	  .then(function(response) {
	    return response.text();
	  }).then(function(response) {
		  //console.log(response);
		  //menu.innerHTML = response;
		  target.innerHTML = response;
	  }).catch(function(error){
		  console.log("error: " + error);
	  });
}

function checkQueryString() {
	let ex = getParameterByName("example");
	
	if (ex != null && ex != undefined && isNumber(ex)) {
		index = ex;
		goToIndex();
	}
}

function addEventListeners() {
	textareaArr.forEach(function(item, index, array) {
		item.addEventListener("keydown", function(e){
			if (e.keyCode == 9) { // tab
				e.preventDefault();
				let ta = e.target; 
			    var start = this.selectionStart;
			    var end = this.selectionEnd;

			    // set textarea value to: text before caret + tab + text after caret
			    ta.value = ta.value.substring(0, start)
			                + "\t"
			                + ta.value.substring(end);

			    // put caret at right position again
			    this.selectionStart = this.selectionEnd = start + 1;
			}
		});
	});
	
	if (inputElem != "undefined" && inputElem != null) {
		inputElem.addEventListener("keydown", function(e){
			if (e.keyCode == 13) { // enter
				goToIndex();
			}
		});		
	} else {
		//console.log("inputElem is undefined or null!");
	}
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function goToIndex() {	
	if (!inputElem) {
		inputElem = orly.qid("input");	
	}
	
	let newIndex = inputElem.value;
	
	if (isNumber(newIndex)) {
		let num = +newIndex;
		
		if (num > 0 && num <= exampleSize) {
			index = (num-2);
			proceed("next", nextElem);
		}
	}
}

function isNumber(str) {
	return !isNaN(str);
}

function initializeArrays() {
	textareaArr = orly.qa("textarea");
	editorArr = orly.qa(".dialog-editor");
}

function setLastExample() {
	//let divs = orly.qa("[id^='ex-']");
	//exampleSize = divs.length;
	//console.log("exampleSize=" + exampleSize);
}

function proceed(direction, element) {	
	if (element.getAttribute('disabled') == "true") {
		return;
	}
	
	if ("next" == direction && ((index+1) > exampleSize)) {
		return;
	}
	
	if ("prev" == direction && ((index-1) < 0)) {
		return;
	}
	
	// If we have a "good" click then clear the input and output
	outputElem.innerHTML="";
	inputElem.value="";
	
	if (prevElem == "undefined" || prevElem == null) {
		prevElem = orly.qid("prev");
	}
	
	if (nextElem == "undefined" || nextElem == null) {
		nextElem = orly.qid("next");
	}
	
	if ("prev" == direction) {
		// Decrement index
		index-=1;
		
		// Always show "next" button on prev action
		nextElem.removeAttribute("disabled");
		
		// If we reach the first example disable prev btn
		if (index == 0) {
			prevElem.setAttribute("disabled", true);	
		}
	} else if ("next" == direction) {
		// Increment index
		index+=1;
		
		// Always show "next" button on prev action
		prevElem.removeAttribute("disabled");
		
		// If we reach the last example disable next btn
		if (index == (exampleSize-1)) {
			nextElem.setAttribute("disabled", true);
		}
	}
	
	// Hide current example
	hideAndShowExamples(index);
	
	// Change heading value
	updateHeadingValue();
}

function hideAndShowExamples(currentIndex) {
	textareaArr.forEach(function(item, index, array) {
		if (index == currentIndex) {
			item.classList.remove("display-none");
			item.classList.remove("d-none");
			adjustTextareas();
		} else {
			item.classList.add("display-none");
			item.classList.remove("d-none");
		}
	});
}

function updateHeadingValue() {
	let heading = orly.qid("heading");
	
	if (heading != undefined && heading != null) {
		if ((index+1) <= exampleSize) {
			heading.innerHTML = "Example " + (index+1) + " of " + exampleSize;	
		}		
	} else {
		console.log("heading is undefined or null!");
	}
}

function intializePrevAndNext() {
	if (exampleSize <= 1) {
		// disable both
		prevElem.setAttribute("disabled", true);
		nextElem.setAttribute("disabled", true);		
	} else {
		prevElem.setAttribute("disabled", true);
		nextElem.removeAttribute("disabled");
	}
}

function adjustTextareas() {
	var textareaList = document.getElementsByTagName("textarea");
	
	for (let ta of textareaList) {
		textAreaAdjust(ta);
	} 
}

function textAreaAdjust(ta) {
	ta.style.height = "1px";
	ta.style.height = (10+ta.scrollHeight) + "px";
}

function print(response) {
	output.innerHTML += (response + "<br/>");
}

function println(response) {
	output.innerHTML += (response + "<br/><br/>");
}

/*function printMirror(response) {
	//output.innerHTML += "document.write(" + response + " = " + response + "<br/>");";
	output.innerHTML = "1 < 10 = " + (1 < 10);
}
*/
function printObject(prefix, response) {
	output.innerHTML += (prefix + " " + JSON.stringify(response) + "<br/>");
}

function printResponse(response, outputTarget) {
	let div = orly.qid(outputTarget);
	div.innerHTML += (response + "<br/>");
}

function reset(target) {
	orly.qid(target).innerHTML = "";
}

function resetElement(element) {
	element.innerHTML = "";
}

/**
 * Note that you must create a new element because in HTML5, each 
 * script element has an associated flag to indicate whether it's 
 * been executed and it can only be executed once. Replacing the 
 * content doesn't reset the flag, and a cloned script element keeps 
 * the setting of the element it was cloned from.
 */
function run(input, output) {
    var el = orly.qid(input);
    var scriptText = el.value;
    var oldScript = orly.qid('scriptContainer');
    var newScript;

    reset(outputElem);
    
    setTimeout(function() {
	    if (oldScript) {
		      oldScript.parentNode.removeChild(oldScript);
	    }

	    newScript = document.createElement('script');
	    newScript.id = 'scriptContainer';
	    newScript.text = el.value;
	    document.body.appendChild(newScript);
	}, 400);
} 

function runJS() {
    var element = textareaArr[index];
    var scriptText = element.value;
    var oldScript = orly.qid('scriptContainer');
    var newScript;

    resetElement(outputElem);
    
    setTimeout(function() {
	    if (oldScript) {
	    	try {
	    		oldScript.parentNode.removeChild(oldScript);	
	    	} catch (err) {/* swallow error */}
	    }

	    newScript = document.createElement('script');
	    newScript.id = 'scriptContainer';
	    newScript.text = "try { \n";
	    newScript.text += element.value;
	    newScript.text += "} catch (err) {\n";
	    newScript.text += "	//alert(err); \n";
	    newScript.text += "orly.qid('alerts').createAlert({type:'danger', msg:err});\n";
	    newScript.text += "}";	    
	    document.body.appendChild(newScript);
	}, 400);
} 

function getError(errorObject) {
	if (errorObject == null || errorObject == "undefined") {
		return "Unknown Exception";
	} else {
		let status = errorObject.status;
		let text = errorObject.statusText;
		console.log(status + " " + text);
		return "Server Error. Status="+status+" Msg="+text;
	}
}

